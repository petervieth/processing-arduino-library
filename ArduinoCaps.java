import processing.core.PApplet;
import processing.serial.Serial;

public class ArduinoCaps {
  private int numAnalogPins;
  private int numPins;
  private int numDigitalPins;
  
  /**
   * Create an Arduino capabilities object.
   *
   * @param boardName The type of board attached.
   */
  public ArduinoCaps(String boardName) {
    // Mega boards
    if(boardName.equals("mega2560") || boardName.equals("mega1280")) {
      this.numAnalogPins=16;
      this.numPins=70;
      this.numDigitalPins=54;
    }  // Arduino Duemilanove, Diecimila, and NG
    else if(boardName.equals("mega168") || boardName.equals("mega328P")) {
      this.numAnalogPins=6;
      this.numPins=20;
      this.numDigitalPins=14;
    } // Teensy 2.0
    else if(boardName.equals("teensy2")) {
      this.numAnalogPins=12;
      this.numPins=25;
      this.numDigitalPins=11;
    } 
  }
  
  public int getNumAnalogPins() {
    return this.numAnalogPins;
  }
  
  public int getNumDigitalPins() {
    return this.numDigitalPins;
  }
  
  public int getNumPins() {
    return this.numPins;
  }
}
